import React from 'react'

const App = () => {
  return (
    <h1 className="text-3xl font-bold from-neutral-100 underline">
    Hello world!
  </h1>
  )
}

export default App